package element;

import org.openqa.selenium.*;
import webdriver.MtlDriver;

import java.util.List;

public class MtlElement implements WebElement {

    By by;
    MtlDriver driver;

    public MtlElement(By by, MtlDriver driver) {
        this.by = by;
        this.driver =driver;
    }

    @Override
    public void click() {
        find(this.by).click();
    }

    @Override
    public void submit() {
        find(this.by).submit();
    }

    @Override
    public void sendKeys(CharSequence... keysToSend) {
        find(this.by).sendKeys(keysToSend);
    }

    @Override
    public void clear() {
        find(this.by).clear();
    }

    @Override
    public String getTagName() {
        return find(this.by).getTagName();
    }

    @Override
    public String getAttribute(String name) {
        return find(this.by).getAttribute(name);
    }

    @Override
    public boolean isSelected() {
        return find(this.by).isSelected();
    }

    @Override
    public boolean isEnabled() {
        return find(this.by).isEnabled();
    }

    @Override
    public String getText() {
        return find(this.by).getText();
    }

    @Override
    public List<WebElement> findElements(By by) {
        return null;
    }

    @Override
    public WebElement findElement(By by) {
        return null;
    }

    @Override
    public boolean isDisplayed() {
        return find(this.by).isDisplayed();
    }

    @Override
    public Point getLocation() {
        return find(this.by).getLocation();
    }

    @Override
    public Dimension getSize() {
        return find(this.by).getSize();
    }

    @Override
    public Rectangle getRect() {
        return find(this.by).getRect();
    }

    @Override
    public String getCssValue(String propertyName) {
        return find(this.by).getCssValue(propertyName);
    }

    @Override
    public <X> X getScreenshotAs(OutputType<X> target) throws WebDriverException {
        return find(this.by).getScreenshotAs(target);
    }

    private WebElement find (By selector) {

        return this.driver.findElement(selector);

    }
}
