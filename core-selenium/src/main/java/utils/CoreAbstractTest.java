package utils;

import java.util.concurrent.ConcurrentHashMap;

//Abstract test class for use with all tests, providers core cross browser & core properties handling
public abstract class CoreAbstractTest {

    private static CoreProperties getProperties() {
        return new CoreProperties("test.properties");
    }

    protected static String getTestProperty(String propertyName) {
        CoreProperties properties = getProperties();

        return properties.getProperties().getProperty(propertyName);
    }

    protected ConcurrentHashMap<String, Object> putOption(ConcurrentHashMap<String, Object> map, String key, Object value) {
        map.putIfAbsent(key, value);

        return map;
    }

    protected ConcurrentHashMap<String, Object> removeOption(ConcurrentHashMap<String, Object> map, String key, Object value) {
        map.remove(key, value);

        return map;
    }

    protected ConcurrentHashMap<String, Object> clearMap(ConcurrentHashMap<String, Object> map) {
        map.clear();

        return map;
    }
}
