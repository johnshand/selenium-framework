package utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Properties;

//reads the contents of java properties files for use throughout core & test
public class CoreProperties {

    String fileName;
    Properties properties;
    static Logger logger = LogManager.getLogger(CoreProperties.class);

    //requires a .properties file name as a String
    public CoreProperties(String fileName) {
        this.fileName = fileName;
    }

    //get's the properties in the file declared
    public Properties getProperties() {
        try {
            properties = new Properties();

            InputStream stream;

            stream = getClass().getClassLoader().getResourceAsStream(this.fileName);

            if(stream != null) {
                properties.load(stream);
            } else {
                throw new FileNotFoundException("\nFile with name of " + this.fileName + "does not exists\n");
            }

        } catch (Exception e) {
            logger.error(e);
        }

        return properties;
    }
}
