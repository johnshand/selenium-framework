package webdriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import utils.CoreProperties;
import webdriver.manage.browsers.BrowserType;
import webdriver.manage.DriverManager;
import webdriver.manage.DriverManagerFactory;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class MtlDriver extends RemoteWebDriver implements WebDriver {

    DriverManager driverManager;
    WebDriver driver;

    public static class Builder {
        BrowserType browser = BrowserType.CHROME;
        Map<String, Object> options;
        String path;

        public Builder withBrowser(BrowserType browser) {
            this.browser = browser;
            return this;
        }

        public Builder withOptions(Map<String, Object> options) {
            this.options = options;
            return this;
        }

        public Builder withPath(String path) {
            this.path = path;
            return this;
        }

        public MtlDriver build() {
            return new MtlDriver(this);
        }
    }

    private MtlDriver(Builder builder) {
        this.driverManager = DriverManagerFactory.getDriverManager(builder.browser);
        this.driverManager.startDriverService();
        this.driverManager.createWebDriver(builder.options);
        this.driver = this.driverManager.getDriver();
        this.driver.get(baseUrl() + builder.path);
    }

    protected String baseUrl() {
        CoreProperties properties = getProperties();

        return properties.getProperties().getProperty("baseUrl");
    }

    private CoreProperties getProperties() {
        return new CoreProperties("test.properties");
    }
    
    @Override
    public void get(String url) {
       this.driver.get(url);
    }

    @Override
    public String getCurrentUrl() {
        return this.driver.getCurrentUrl();
    }

    @Override
    public String getTitle() {
        return this.driver.getTitle();
    }

    @Override
    public List<WebElement> findElements(By by) {
        return this.driver.findElements(by);
    }

    @Override
    public WebElement findElement(By by) {
        return this.driver.findElement(by);
    }

    @Override
    public String getPageSource() {
        return this.driver.getPageSource();
    }

    @Override
    public void close() {
        this.driver.close();
    }

    @Override
    public void quit() {
        this.driver.quit();
    }

    @Override
    public Set<String> getWindowHandles() {
        return this.driver.getWindowHandles();
    }

    @Override
    public String getWindowHandle() {
        return this.driver.getWindowHandle();
    }

    @Override
    public TargetLocator switchTo() {
        return this.driver.switchTo();
    }

    @Override
    public Navigation navigate() {
        return this.driver.navigate();
    }

    @Override
    public Options manage() {
        return this.driver.manage();
    }

    public DriverManager getManager() {
        return this.driverManager;
    }

    public void tearDown() {
        this.driver.quit();

        this. driverManager.stopDriverService();
    }
}
