package webdriver.manage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import utils.CoreProperties;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Objects;

public abstract class DriverManager implements DriverManagerInterface {

    protected WebDriver driver;

    CoreProperties properties;
    String binaryName;
    String path;
    File binary;
    static Logger logger = LogManager.getLogger(DriverManager.class);

    //selenium grid hub url
    private URL gridUrl;

    //gets grid URL from test.properties
    public URL getGridUrl() {
        properties = new CoreProperties("test.properties");

        String url = properties.getProperties().getProperty("gridUrl");

        try {
            gridUrl = new URL(url);
        } catch (final MalformedURLException e) {
            logger.fatal(e + "\n" + "URL: " + url + " is not valid\n");
        }

        return gridUrl;
    }

    public File getBinary(String name) {
        CoreProperties properties = new CoreProperties("test.properties");

        binaryName = properties.getProperties().getProperty(name);

        try {
            path = Objects.requireNonNull(getClass().getClassLoader().getResource(binaryName)).getFile();
        } catch (NullPointerException e) {
            logger.fatal(e + "\n" + "WebDriver executable Binary File '" + binaryName +"' not found\n");
        }

        binary = new File(path);

        return binary;
    }

    //Allows access to the created WebDriver object
    public final WebDriver getDriver() {
        //If no driver object has been created, one will be with all defaults
        if(driver == null) {
            startDriverService();
            createWebDriver(new HashMap<>());
            logger.warn("\n No driver existed. A default driver was created for you.\n");
        }

        return driver;
    }
}
