package webdriver.manage.browsers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.GeckoDriverService;
import org.openqa.selenium.remote.RemoteWebDriver;
import webdriver.manage.DriverManager;
import webdriver.manage.DriverManagerInterface;

import java.io.File;
import java.io.IOException;
import java.util.Map;

public class FirefoxDriverManager extends DriverManager implements DriverManagerInterface {

    GeckoDriverService geckoDriverService;
    File geckoDriverBinary;
    String binaryPropertyName = "ffBinary";
    static Logger logger = LogManager.getLogger(FirefoxDriverManager.class);

    public FirefoxDriverManager() {
        this.geckoDriverBinary = getBinary(binaryPropertyName);
    }

    @Override
    public void startDriverService() {
        if(null == geckoDriverService) {
            try {
                geckoDriverService = new GeckoDriverService.Builder()
                        .usingDriverExecutable(geckoDriverBinary)
                        .usingAnyFreePort()
                        .build();
                geckoDriverService.start();
            } catch (IOException e) {
                logger.error(e);
            }
        }
    }

    @Override
    public void stopDriverService() {
        if(null != geckoDriverService && geckoDriverService.isRunning()) {
            geckoDriverService.stop();
        }
    }

    @Override
    public final void createWebDriver(Map<String, Object> options) {
        FirefoxOptions firefoxOptions = new FirefoxOptions();

        if(!options.isEmpty()) {
            options.forEach(firefoxOptions::setCapability);
        }

        this.driver = new RemoteWebDriver(getGridUrl(), firefoxOptions);
    }
}
