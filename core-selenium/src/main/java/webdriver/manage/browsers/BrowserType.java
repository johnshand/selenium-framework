package webdriver.manage.browsers;

//enum list of supported browsers for use in tests when calling for an instance of a WebDriver
public enum BrowserType {
    CHROME,
    FIREFOX
}
