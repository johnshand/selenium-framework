package webdriver.manage.browsers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import webdriver.manage.DriverManager;
import webdriver.manage.DriverManagerInterface;

import java.io.File;
import java.io.IOException;
import java.util.Map;

public class ChromeDriverManager extends DriverManager implements DriverManagerInterface {

    ChromeDriverService chromeDriverService;
    File chromeDriverBinary;
    String binaryPropertyName = "chromeBinary";
    static Logger logger = LogManager.getLogger(ChromeDriverManager.class);

    //constructor handles identifying driver binary variables
    public ChromeDriverManager() {
        this.chromeDriverBinary = getBinary(binaryPropertyName);
    }

    @Override
    public void startDriverService() {
        if(null == chromeDriverService) {
            try {
                chromeDriverService = new ChromeDriverService.Builder()
                        .usingDriverExecutable(chromeDriverBinary)
                        .usingAnyFreePort()
                        .build();
                chromeDriverService.start();
            } catch (IOException e) {
                logger.error(e);
            }
        }
    }

    @Override
    public void stopDriverService() {
        if(null != chromeDriverService && chromeDriverService.isRunning()) {
            chromeDriverService.stop();
        }
    }

    @Override
    public final void createWebDriver(Map<String, Object> options) {
        ChromeOptions chromeOptions = new ChromeOptions();

        if(!options.isEmpty()) {
            options.forEach(chromeOptions::setCapability);
        }

        this.driver = new RemoteWebDriver(getGridUrl(), chromeOptions);
    }
}
