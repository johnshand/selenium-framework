package webdriver.manage;

import java.io.File;
import java.net.URL;
import java.nio.file.NoSuchFileException;
import java.util.Map;

public interface DriverManagerInterface {

    //to be implemented in each browser specific driver manager class to start it's respective service
    void startDriverService();

    void stopDriverService();

    //implemented in driverManager and overridden in browser specific classes
    void createWebDriver(Map<String, Object> options);

    //to get the grid url for use in the browser specific RemoteWebDriver instance
    URL getGridUrl();

    //to get the binary executable for the appropriate browser
    File getBinary(String name) throws NoSuchFileException;
}
