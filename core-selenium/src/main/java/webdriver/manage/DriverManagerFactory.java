package webdriver.manage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import webdriver.manage.browsers.BrowserType;
import webdriver.manage.browsers.ChromeDriverManager;
import webdriver.manage.browsers.FirefoxDriverManager;

//Instantiates the correct DriverManager object as requested by a test using the BrowserType Enum
public class DriverManagerFactory {

    static DriverManager driverManager;
    static Logger logger = LogManager.getLogger(DriverManagerFactory.class);

    //Method for use in tests, accepts an enum variable of BrowserType
    public static DriverManager getDriverManager(BrowserType type) {
        //this switch instantiates the relevant browser specific DriveManager object to be returned
        try {
            switch (type) {
                case FIREFOX:
                    driverManager = new FirefoxDriverManager();
                    break;
                case CHROME:
                    driverManager = new ChromeDriverManager();
                    break;
                default:
                    throw new IllegalArgumentException("\nInvalid Browser Type. " + type + " is invalid or unsupported\n");
            }
        } catch (Exception e) {
            logger.fatal(e);
        }

        return driverManager;
    }
}
