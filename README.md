##### Pre-Requisites

* To run this project ensure you have a compatible version of Java set up locally
* Docker must be installed locally

##### Project

* This project was created using JDK: 1.8 Version: 1.8.0_221

* Dependencies are managed using gradle version: 6.0.1

* The Project is split into two modules;
  * core-selenium (Operates as a library of commonly used WebDriver capabilities)
  * tests (includes tests, page objects & test.properties)
  
* The project runs against selenium grid hosted in docker. The docker-compose.yml in the core-selenium root directory manages the spinning up of the grid
  * Grid nodes run linux with the latest version of chrome and/ or firefox for the purpose of this project
  
##### core-selenium

The 'core-selenium' module contains files which manage Selenium WebDriver (A.K.A MtlDriver) Instances for use across all test files.
It also contains utility classes & the CoreAbstractTest class.

###### DriverManager

The DriverManager abstract class implements the DriverManagerInterface defines the core behaviours of Driver Management
* getBinary - gets the browser specific binary as required
* getDriver - returns the instance of the created driver
* getGridUrl - gets the selenium grid URL from test.properties

These browser specific DriverManagers are located in the 'webdriver.browsers' package. A class is required for each browser supported.

Currently Chrome (ChromeDriverManager) & Firefox (FirefoxDriverManager) are supported. **Browser specific options are managed here**.

###### BrowserType

ENUM variables for BrowserType are declared in the same package and should align with supported browsers. Currently there is;

* CHROME
* FIREFOX

The default browser is CHROME, it is declared in the MtlDriver.Builder. It can be custom set with a call toi MtlDriver.Builder.withBrowser().

###### DriverManagerFactory 

Is the class which creates Browser Specific Driver Manager instances on request and allows access to the DriverManager via it's getDriverManger() method.

getDriverManager() accepts a variable of the enum BrowserType (above);

###### CoreAbstractTest

**All Tests Should Extend This Class**

The CoreAbstractTest provides functionality to Test classes such as access to properties stored in test.properties & utils for managing browser option maps.

###### MtlDriver

Is a custom created WebDriver object. It provides the same functionality as the well known WebDriver with the addition of a builder pattern.
The pattern allows quick and simple WebDriver creation for any supported browser with custom options and a definable path.

It also contains a tearDown method for post-test instance removal.

Each test should create an instance of this object with the required config. 

###### MtlElement  

WebElement class object that takes a driver and a By object element identifier and produces a specific element object. The usual WebElement methods can then be called.

Build all elements in the page class constructor for simplified/ easier read access throughout the page class

###### Implementation

Combined, core-selenium objects allow tests to simply access a MtlDriver instance for whatever browser they wish to use.

##### Tests

Tests & associated files are located in the 'tests' module. 

Tests are ran using Junit5 (jupiter) implementations.

Test methods must be annotated with @Test or similar

Test classes should extend CoreAbstractTest for easy access to test.properties

Best practice for Test methods is to indicate a test name, this allows for a string descriptor to be added to each test,
output is therefore more human readable. adding the 'name' option to the browser instance allows for passing of test names to selenium grid instances.

Tests can be run single or cross browser/ platform, simply call any combination of withBrowser() / withOptions() / withPath() from the mtlDriver Builder. on Build() this will then;
* set the DriverManager object & relevant browser options
* get the DriverManager object
* start the DriverService
* get the Driver to run the test

For cross browser testing, write one test per browser required, create methods where possible to handle any duplicated code (DRY)
Be sure however to not make the tests too DRY as they still need to be easily read.

Junit @ParameterizedTest & @EnumSource combined enables a single test to be ran in multiple browsers without duplicated the test code

`@ParameterizedTest(name = "User with valid credentials Can Sign In in {0}")`
`@EnumSource(value = BrowserType.class, names = {"CHROME", "FIREFOX"})`
`void validSignIn(BrowserType browser) {
        setUpBrowser(browser, optionsMap)...;`
        
A test.properties file should be created in the resources directory of your test root directory. It should contain all common use properties.

A few mandatory propertis must be set

baseUrl - your test base url, MtlDriver will automatically point tests at this baseUrl + and path you pass withPath()
gridUrl - the url of the selenium grid instance your tests will run against
chromeBinary - the name of the Chrome Driver Binary File
ffBinary - the name of the Gecko Driver Binary file

Driver binaries must be stored in the resources directory of your test root directory.

###### Page Object Model

Tests are structured according to a Page Object model. This means;

* Each Page of the AUT (Application Under Test) has it's own class allowing instantiation of the Page Object (PO)

* Any Test can access any PO for formation of a test, no need to duplicate code

* Each PO class should contain;
    * a constructor accepting an instance of WebDriver
    * identifiers for each element interacted with on that page
    * methods for mtlDriver interaction with the page
    * any supporting methods as require

* Assertions should NOT exist in a PO, tests are easier to read this way

###### Local Running

First you must start the selenium grid;
* Start docker
* run 'docker-compose up -d' from the core-selenium directory in the terminal

Docker runs the debug containers for chrome & firefox which allows GUI viewing via a VNC viewer **Optional**;
* Install a VNC Viewer
* Ports 4445 (firefox) & 4446 (chrome)
* default password is 'secret'

For local running Browser specific mtlDriver binaries are located in **tests/src/test/resources** and are as follows.
* chromedriver
* geckodriver

To execute the tests after following the above steps run;
 * Mac os/ Linux: ./gradlew clean test
 * windows: gradlew clean test
 
###### Reporting

Reporting is handled by junit's built in reporter. Test run out put can be found post execution in tests/build/reports/tests/test.

index.html can be opened in the browser.

Allure is currently in R&D for further reporting functionality;
Run from project root:
docker run -p 4040:4040 -p 5050:5050 -e CHECK_RESULTS_EVERY_SECONDS=3 -e KEEP_HISTORY="TRUE" -v ${PWD}/tests/allure-results:/app/allure-results frankescobar/allure-docker-service
