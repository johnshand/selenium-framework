import enums.FormTextInputField;
import enums.Priorities;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import pages.*;
import webdriver.MtlDriver;
import webdriver.manage.browsers.BrowserType;
import utils.CoreAbstractTest;

import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

import static org.junit.jupiter.api.parallel.ExecutionMode.CONCURRENT;

@Execution(CONCURRENT)
class FeedbackFormTest extends CoreAbstractTest {

    private MtlDriver driver;
    private SignInPage signInPage;
    private FeedbackForm feedbackForm;
    private ThankYouPage thankYouPage;
    private ConcurrentHashMap<String, Object> browserOptions = new ConcurrentHashMap<String, Object>();
    private static final String firstName = "John";
    private static final String lastName = "Shand";
    private static final String email = "js@fakemail.void";
    private static final String phoneNumber = "07777777777";
    private static final String company = "Hopefully Matillion";
    private static final String postcode = "SK1 1AA";
    private static final String feedbackText = "This is some feedback.";
    private static final String nameKey = "name";

    @ParameterizedTest(name = "User can enter valid text data into {1} field in Chrome")
    @MethodSource("textInputDataProvider")
    void textInputTest(String text, FormTextInputField field) {

        putOption(browserOptions, nameKey, "User can enter input data into " + field + " in Chrome");

        driver = new MtlDriver.Builder()
                .withBrowser(BrowserType.CHROME)
                .withOptions(browserOptions)
                .withPath("/")
                .build();

        signInPage = new SignInPage(driver)
                .signIn();

        feedbackForm = new FeedbackForm(driver);

        feedbackForm.inputText(text, field);

        Assertions.assertEquals(text, feedbackForm.getValue(field));
    }

    private static Stream<Arguments> textInputDataProvider() {

        return Stream.of(
                Arguments.of(firstName, FormTextInputField.FIRSTNAME),
                Arguments.of(lastName, FormTextInputField.LASTNAME),
                Arguments.of(email, FormTextInputField.EMAIL),
                Arguments.of(phoneNumber, FormTextInputField.PHONE),
                Arguments.of(company, FormTextInputField.COMPANY),
                Arguments.of(postcode, FormTextInputField.POSTCODE),
                Arguments.of(feedbackText, FormTextInputField.FEEDBACK)
        );
    }

    @ParameterizedTest(name = "User can Select {0} from drop down in Chrome")
    @EnumSource(Priorities.class)
    void dropDownTest(Priorities priority) {

        putOption(browserOptions, nameKey, "User can Select " + priority + " from drop down in Chrome");

        driver = new MtlDriver.Builder()
                .withBrowser(BrowserType.CHROME)
                .withOptions(browserOptions)
                .withPath("/")
                .build();

        signInPage = new SignInPage(driver)
                .signIn();

        feedbackForm = new FeedbackForm(driver);

        feedbackForm.setPriority(priority);

        Assertions.assertEquals(priority.name(), feedbackForm.getSelectedPriority().toUpperCase());
    }

    @ParameterizedTest(name = "User can fill in and submit form in {0}")
    @EnumSource(value = BrowserType.class, names = {"CHROME", "FIREFOX"})
    void submitFormTest(BrowserType browser) {

        putOption(browserOptions, nameKey, "User can fill in and submit form in " + browser);

        driver = new MtlDriver.Builder()
                .withBrowser(browser)
                .withOptions(browserOptions)
                .withPath("/")
                .build();

        driver.get("https://www.matillion-automation-test.com/");

        signInPage = new SignInPage(driver)
                .signIn();

        feedbackForm = new FeedbackForm(driver);

        feedbackForm.inputText(firstName, FormTextInputField.FIRSTNAME);
        feedbackForm.inputText(lastName, FormTextInputField.LASTNAME);
        feedbackForm.inputText(email, FormTextInputField.EMAIL);
        feedbackForm.inputText(phoneNumber, FormTextInputField.PHONE);
        feedbackForm.inputText(company, FormTextInputField.COMPANY);
        feedbackForm.inputText(postcode, FormTextInputField.POSTCODE);
        feedbackForm.setPriority(Priorities.NORMAL);
        feedbackForm.inputText(feedbackText, FormTextInputField.FEEDBACK);
        feedbackForm.clickSubmit();

        thankYouPage = new ThankYouPage(driver);

        Assertions.assertTrue(thankYouPage.thankYouIsVisible());
    }

    @ParameterizedTest(name = "Email must include @ symbol and it not be last character (Data Value index - {index})")
    @ValueSource(strings = {"jsfakemail.com", "jsfakemail@"})
    void emailInputValidationTest(String invalidEmail) {

        putOption(browserOptions, nameKey, "User cannot submit form with invalid email: " + invalidEmail);

        driver = new MtlDriver.Builder()
                .withBrowser(BrowserType.CHROME)
                .withOptions(browserOptions)
                .withPath("/")
                .build();

        signInPage = new SignInPage(driver)
                .signIn();

        feedbackForm = new FeedbackForm(driver);

        feedbackForm.inputText(firstName, FormTextInputField.FIRSTNAME);
        feedbackForm.inputText(lastName, FormTextInputField.LASTNAME);
        feedbackForm.inputText(invalidEmail, FormTextInputField.EMAIL);
        feedbackForm.inputText(phoneNumber, FormTextInputField.PHONE);
        feedbackForm.inputText(company, FormTextInputField.COMPANY);
        feedbackForm.inputText(postcode, FormTextInputField.POSTCODE);
        feedbackForm.setPriority(Priorities.NORMAL);
        feedbackForm.inputText(feedbackText, FormTextInputField.FEEDBACK);

        Assertions.assertFalse(feedbackForm.isSubmitEnabled());
    }

    @ParameterizedTest(name = "Phone number boundary test, min/max 11 (Data Value index - {index})")
    @ValueSource(strings = {"0777777777", "077777777777"})
    void phoneInputValidationTest(String invalidPhoneNumber) {

        putOption(browserOptions,nameKey, "User cannot submit form with invalid phone number: " + invalidPhoneNumber);

        driver = new MtlDriver.Builder()
                .withBrowser(BrowserType.CHROME)
                .withOptions(browserOptions)
                .withPath("/")
                .build();

        signInPage = new SignInPage(driver)
                .signIn();

        feedbackForm = new FeedbackForm(driver);

        feedbackForm.inputText(firstName, FormTextInputField.FIRSTNAME);
        feedbackForm.inputText(lastName, FormTextInputField.LASTNAME);
        feedbackForm.inputText(email, FormTextInputField.EMAIL);
        feedbackForm.inputText(invalidPhoneNumber, FormTextInputField.PHONE);
        feedbackForm.inputText(company, FormTextInputField.COMPANY);
        feedbackForm.inputText(postcode, FormTextInputField.POSTCODE);
        feedbackForm.setPriority(Priorities.NORMAL);
        feedbackForm.inputText(feedbackText, FormTextInputField.FEEDBACK);

        Assertions.assertFalse(feedbackForm.isSubmitEnabled());
    }

    @ParameterizedTest(name = "Postcode must match regex. (Data Value index - {index})")
    @ValueSource(strings = {"SK1 1A", "SK1 1A3"})
    void postcodeValidationTest(String invalidPostcode) {

        putOption(browserOptions, nameKey, "User cannot submit form with invalid postcode: " + invalidPostcode);

        driver = new MtlDriver.Builder()
                .withBrowser(BrowserType.CHROME)
                .withOptions(browserOptions)
                .withPath("/")
                .build();

        signInPage = new SignInPage(driver)
                .signIn();

        feedbackForm = new FeedbackForm(driver);

        feedbackForm.inputText(firstName, FormTextInputField.FIRSTNAME);
        feedbackForm.inputText(lastName, FormTextInputField.LASTNAME);
        feedbackForm.inputText(email, FormTextInputField.EMAIL);
        feedbackForm.inputText(phoneNumber, FormTextInputField.PHONE);
        feedbackForm.inputText(company, FormTextInputField.COMPANY);
        feedbackForm.inputText(invalidPostcode, FormTextInputField.POSTCODE);
        feedbackForm.setPriority(Priorities.NORMAL);
        feedbackForm.inputText(feedbackText, FormTextInputField.FEEDBACK);

        Assertions.assertFalse(feedbackForm.isSubmitEnabled());
    }

    @AfterEach
    void tearDown() {

        clearMap(browserOptions);

        driver.quit();

        driver.getManager().stopDriverService();

    }
}
