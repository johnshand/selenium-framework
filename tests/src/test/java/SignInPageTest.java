import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.MethodSource;
import enums.Credentials;
import pages.FeedbackForm;
import pages.SignInPage;
import utils.CoreAbstractTest;
import org.junit.jupiter.api.*;
import webdriver.MtlDriver;
import webdriver.manage.browsers.BrowserType;

import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

import static org.junit.jupiter.api.parallel.ExecutionMode.CONCURRENT;

@Execution(CONCURRENT)
class SignInPageTest extends CoreAbstractTest {

    private MtlDriver driver;
    private SignInPage signInPage;
    private FeedbackForm feedbackForm;
    private ConcurrentHashMap<String, Object> browserOptions = new ConcurrentHashMap<String, Object>();
    private static final String invalidUsername = "v.jenkins";
    private static final String invalidPassword = "hunter3";
    private static final String nameKey = "name";

    @ParameterizedTest(name = "User with valid credentials Can Sign In in {0}")
    @EnumSource(value = BrowserType.class, names = {"CHROME", "FIREFOX"})
    void validSignIn(BrowserType browser) {

        putOption(browserOptions, nameKey, "User with valid credentials can sign in");

        driver = new MtlDriver.Builder()
                .withBrowser(browser)
                .withOptions(browserOptions)
                .withPath("/")
                .build();

        signInPage = new SignInPage(driver)
                .inputCredential(Credentials.USERNAME)
                .inputCredential(Credentials.PASSWORD)
                .clickSignIn();

        feedbackForm = new FeedbackForm(driver);

        Assertions.assertTrue(feedbackForm.isFormLoaded());
    }

    @ParameterizedTest(name = "User cannot sign in with {0} and helper text is displayed correctly")
    @MethodSource("credentialDataProvider")
    void invalidSignIn(String desc, String username, String password) {

        putOption(browserOptions, nameKey, "User cannot sign in with " + desc + ". Helper text will be displayed");

        driver = new MtlDriver.Builder()
                .withBrowser(BrowserType.CHROME)
                .withOptions(browserOptions)
                .withPath("/")
                .build();

        signInPage = new SignInPage(driver)
                .inputUsername(username)
                .inputPassword(password)
                .clickSignIn();

        Assertions.assertTrue(signInPage.passwordHelperTextIsVisible());
        Assertions.assertEquals("Please enter a valid username/password", signInPage.getPasswordHelperText());
    }

    private static Stream<Arguments> credentialDataProvider() {

        return Stream.of(
                Arguments.of("invalid username", invalidUsername, getTestProperty("password")),
                Arguments.of("invalid password", getTestProperty("username"), invalidPassword),
                Arguments.of("invalid username and password", invalidUsername, invalidPassword)
        );
    }
    
    @AfterEach
    void tearDown() {

        clearMap(browserOptions);

        driver.tearDown();

    }
}
