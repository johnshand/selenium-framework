package enums;

import org.openqa.selenium.WebDriver;
import pages.SignInPage;

public enum Pages {

    SIGNIN,
    FEEDBACK,
    THANKYOU
}
