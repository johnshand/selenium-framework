package enums;

public enum FormTextInputField {
    FIRSTNAME,
    LASTNAME,
    EMAIL,
    PHONE,
    COMPANY,
    POSTCODE,
    FEEDBACK

}
