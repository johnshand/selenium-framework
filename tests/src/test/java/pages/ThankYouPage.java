package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import webdriver.MtlDriver;

public class ThankYouPage {

    private MtlDriver driver;

    public ThankYouPage(MtlDriver driver) {
        this.driver = driver;
    }

    //element identifier By objects
    private final By thankYouText = By.cssSelector("#root > div > main > h2");

    @Step("Check if the Thank You page is visible")
    public boolean thankYouIsVisible() {
        return find(thankYouText).isDisplayed();
    }

    //method to find element on page for use in interaction methods above
    private WebElement find (By selector) {
        return this.driver.findElement(selector);
    }
}
