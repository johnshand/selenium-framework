package pages;

import element.MtlElement;
import enums.Credentials;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import utils.CoreAbstractTest;
import webdriver.MtlDriver;

public class SignInPage extends CoreAbstractTest {

    MtlDriver driver;
    final MtlElement userNameInputField;
    final MtlElement passwordInputField;
    final MtlElement signInButton;
    final MtlElement passwordHelper;

    public SignInPage(MtlDriver driver) {
        this.driver = driver;
        this.userNameInputField = getElement(By.id("username"));
        this.passwordInputField = getElement(By.id("password"));
        this.signInButton = getElement(By.cssSelector(".MuiButton-label"));
        this.passwordHelper  = getElement(By.id("password-helper-text"));
    }

    //signIn method to allow tests to sign in when not required to directly test this page
    @Step("Sign In")
    public SignInPage signIn() {

        inputCredential(Credentials.USERNAME);
        inputCredential(Credentials.PASSWORD);
        clickSignIn();

        return this;

    }

    @Step("Input {0} credential")
    public SignInPage inputCredential(Credentials credential) {

        if(credential == Credentials.USERNAME) {
            userNameInputField.sendKeys(getTestProperty("username"));
        } else if (credential == Credentials.PASSWORD) {
            passwordInputField.sendKeys(getTestProperty("password"));
        }

        return this;
    }

    @Step("Input username of {0}")
    public SignInPage inputUsername(String username) {

        userNameInputField.sendKeys(username);

        return this;

    }

    @Step("Input password of {0}")
    public SignInPage inputPassword(String password) {

        passwordInputField.sendKeys(password);

        return this;

    }

    @Step("Click Sign In")
    public SignInPage clickSignIn() {

        signInButton.click();

        return this;

    }

    //Assertion Helper Methods
    @Step("Check that password helper text is visible")
    public boolean passwordHelperTextIsVisible() {

        return passwordHelper.isDisplayed();

    }

    @Step("Get the password helper text")
    public String getPasswordHelperText() {

        return passwordHelper.getText();

    }

    //class helper method
    protected MtlElement getElement(By by) {
        return new MtlElement(by, this.driver);
    }

}
