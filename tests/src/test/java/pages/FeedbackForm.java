package pages;

import enums.FormTextInputField;
import enums.Priorities;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import webdriver.MtlDriver;

public class FeedbackForm {

    private MtlDriver driver;
    static Logger logger = LogManager.getLogger(FeedbackForm.class);

    public FeedbackForm(MtlDriver driver) {
        this.driver = driver;
    }

    //element identifier By objects
    private final By formElement = By.className("feedback-form");
    private final By firstNameInput = By.id("firstname");
    private final By lastNameInput = By.id("lastname");
    private final By emailAddressInput = By.id("email");
    private final By phoneNumberInput = By.id("phone");
    private final By companyInput = By.id("company");
    private final By postcodeInput = By.cssSelector("#postcode");
    private final By priorityDropdown = By.cssSelector("#postcode div select");
    private final By feedbackInput = By.id("feedback");
    private final By submitButton = By.id("submit");

    @Step("Check form is loaded")
    public boolean isFormLoaded() {

        return find(formElement).isDisplayed();

    }

    //simplifies input of text into input fields from tests, requires input test and a input field name enum
    @Step("Input {0} as text into {1}")
    public void inputText(String text, FormTextInputField field){

        keyIntoInput(getInputIdentifier(field), text);
    }

    @Step("Select {0} priority from drop down menu")
    public void setPriority(Priorities priority) {

        Select select = selectPriorityDropDown();
        String priorityText;

        switch (priority) {
            case NORMAL:
                priorityText = "Normal";
                break;
            case HIGH:
                priorityText = "High";
                break;
            case URGENT:
                priorityText = "Urgent";
                break;
            default:
                priorityText = "";

        }
        select.selectByVisibleText(priorityText);
    }

    @Step("Get the currently selected priority in the menu")
    public String getSelectedPriority() {

        return selectPriorityDropDown().getFirstSelectedOption().getText();
    }

    private Select selectPriorityDropDown() {

        return new Select(find(priorityDropdown));
    }

    private String getValueAttribute(WebElement element) {

        return element.getAttribute("value");
    }

    @Step("Get the value attribute of {0}")
    public String getValue(FormTextInputField field) {

        return getValueAttribute(find(getInputIdentifier(field)));
    }

    //serves the correct identifier for enum passed
    private By getInputIdentifier(FormTextInputField field) {

        By formField;

        switch (field) {
            case FIRSTNAME:
                formField = firstNameInput;
                break;
            case LASTNAME:
                formField = lastNameInput;
                break;
            case EMAIL:
                formField = emailAddressInput;
                break;
            case PHONE:
                formField = phoneNumberInput;
                break;
            case COMPANY:
                formField = companyInput;
                break;
            case POSTCODE:
                formField = postcodeInput;
                break;
            case FEEDBACK:
                formField = feedbackInput;
                break;

            default:
                formField = null;
        }

        return  formField;
    }

    @Step("Check if the submit button is enabled")
    public boolean isSubmitEnabled() {
        return find(submitButton).isEnabled();
    }

    @Step("Click the submit button")
    public void clickSubmit() {
        find(submitButton).click();
    }

    private void keyIntoInput(By element, String text) {

        clearAndSendKeys(element, text);
    }

    private void clearAndSendKeys(By element, String text) {

        find(element).clear();

        find(element).sendKeys(text);
    }

    private WebElement find (By selector) {
        return this.driver.findElement(selector);
    }
}
